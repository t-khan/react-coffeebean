<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">

  <h3 align="center">CoffeeBean</h3>

  <p align="center">
    A small attempt to understand ReactJS library. The application displays a menu received from an API call to the menu database.
    <br />
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="mailto:tofikahmad@live.com">Report Bug</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
        <li><a href="#design-process">Design Process</a></li>
        <li><a href="#development-process">Development Process</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

<div align="center">
  <img src="https://imgs.tofikkhan.com/coffee-bean/Design/LogoType.png">
</div>

The project started as a New Year's resolution. By the end of the year 2022, I will learn ReactJS so that I can create efficient and elegant application on my own. I also had an idea of separating my front-end development from my back-end development. The idea is that all data will be requested from an api (api.tofikkhan.com) as a JSON object. The JSON will then be parsed and stored for the application's use.


<div align="center">
  <img src="https://imgs.tofikkhan.com/webservices.png">
</div>


This architecture of application design is neither new nor unique, but it is not something I have done in the past. During this step I learned about the use of CORS and how it can be enabled to allow cross site requests.

In the following weeks I worked on creating the React components and set up a mock menu to pull data from. After that it was just playing around with creating required components and getting the work flow set up.

### Built With

This project is built using:

* [React.js](https://reactjs.org/)
* [PHP](https://nextjs.org/)
* [JQuery](https://jquery.com)
* [useForm](https://react-hook-form.com/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- Design Process -->
## Design Process

Detail of Design process

### Paper Wireframes

*Add Image for paper Wireframes*

### Digital Lo-Fi Wireframes/Prototype

*Add Image and link to Figma LoFi Prototype*

### Digital Hi-Fi Prototype



<p align="right">(<a href="#top">back to top</a>)</p>



<!-- Development Process -->
## Development Process

Detail

### User Journey / Activity Digram

*Insert User Flow Diagram*

### Creating React Application

Description

### Testing

description

### Deployment

description

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [x] Paper Wireframes
- [x] Digital Wireframes
- [x] Hi-Fidelity Prototypes
- [ ] Development
  - [x] Create Basic App with a single React Component
  - [x] Use useState to switch screens
  - [x] Learn how to use fetch() to get data from api
  - [x] Set screen based on return from fetch()
  - [x] Create ItemScreen component
  - [x] Show appropriate item when selected from menu
  - [x] update cart with correct item on submit
  - [ ] style item screen
  - [ ] create ReviewOrderScreen
  - [ ] show cart items on ReviewOrderScreen
  - [ ] style review screen

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

If you have any comments or feedback about this project or want to get in touch, please email me at [tofikahmad@live.com](mailto:tofikahmad@live.com). I look forward to hearing from you. :)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

There is currently no license on this project. If you believe that any bit of code here will help you in your project, please feel free to grab it as needed. I hope you enjoy the project that you're working on.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Your Name - [@your_twitter](https://twitter.com/your_username) - email@example.com

Project Link: [https://github.com/your_username/repo_name](https://github.com/your_username/repo_name)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

This project would not have been finished without the dedicated support and help from these resources:

* [React.Org Documentation](https://reactjs.org/docs/getting-started.html)
* [React Documentation (Beta)](https://beta.reactjs.org/)
* [React Hook Form Documentation](https://react-hook-form.com/get-started)
* [Maksim Ivanov (YouTube)](https://www.youtube.com/channel/UC5hby9iDkwOTQM7PIjyjbgw)
  * [React Hook Form Tutorial](https://www.youtube.com/watch?v=bU_eq8qyjic)
* [Tania Rascia](https://www.taniarascia.com/)
  * [React Tutorial: An Overview and Walkthrough](https://www.taniarascia.com/getting-started-with-react/)
* [JavaScript Tutorial](https://www.javascripttutorial.net/)
  * [3 Ways to Copy Objects in JavaScript](https://www.javascripttutorial.net/object/3-ways-to-copy-objects-in-javascript/)

Additionally, this project would not have been possible without the continued help and support of:
* Delaney McKinley


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
