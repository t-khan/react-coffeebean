import React, { useState, useEffect } from "react";
import logo from "./imgs/logo-black.png";
import "./Menu.css";

function Header() {
  return (
    <div className="menuHeader">
      <img src={logo} className="menuHeaderImg" />
      <h1 className="menuHeaderTitle">CoffeeBean</h1>
    </div>
  );
}

function ItemCard({ item, categoryID, updateScreen, updateShowItem }) {
  return (
    <li key={item.id} className="menuItemList">
      <a
        data-categoryid={categoryID}
        data-itemid={item.id}
        onClick={() => {
          updateScreen("Item");
          updateShowItem(item);
        }}
      >
        <div className="menuItemCard">
          <img src={item.image} className="menuItemImage" />
          <div className="menuItemTitle">{item.title}</div>
          <div className="menuItemPriceContainer">
            <div className="menuItemPrice">${item.price}</div>
          </div>
        </div>
      </a>
    </li>
  );
}

function Category({ category, updateScreen, updateShowItem }) {
  return (
    <li key={"category_" + category.id} className="categoryTitle">
      <div className="menuCategoryTitle">{category.title}</div>
      <ul className="menuCategoryItemsContainer">
        {category.items.map((item) => (
          <ItemCard
            key={item.id}
            item={item}
            categoryID={category.id}
            updateScreen={updateScreen}
            updateShowItem={updateShowItem}
          />
        ))}
      </ul>
    </li>
  );
}

function MenuScreen({ categories, updateScreen, updateShowItem }) {
  /*

  */

  return (
    <>
      <Header />
      <ul className="menuCategoryContainer">
        {categories.map((category, i) => (
          <Category
            key={"mainCategory" + category.id}
            category={category}
            updateScreen={updateScreen}
            updateShowItem={updateShowItem}
          />
        ))}
      </ul>
      <button
        onClick={() => {
          updateScreen("Test");
        }}
      >
        Test
      </button>
      <button
        onClick={() => {
          updateScreen("Review");
        }}
      >
        Cart
      </button>
    </>
  );
}

export default MenuScreen;
