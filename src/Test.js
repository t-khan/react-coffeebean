import React from "react";
import { useForm } from "react-hook-form";

export default function Test({item,updateScreen,cart, updateCart}) {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const onSubmit = (data) => {
    /*
      Executed when add-to-cart-button is clicked
      Iterates over the item.modifiers and item.modifiers.Options
      and removes options that were not selected by the user.
    */

    //create a deep copy of item JSON
    let itemToAdd = JSON.parse(JSON.stringify(item));

    //filter item options based on user's selection
    for (var key in data) {
      for (var i = 0; i < itemToAdd.modifiers.length; i++) {
        if(itemToAdd.modifiers[i].inputName === key) {
          itemToAdd.modifiers[i].options = itemToAdd.modifiers[i].options.filter(option => option.title === data[key]);
        }
      }
    }

    //update cart with the item
    updateCart([...cart, itemToAdd]);
    console.log([...cart, itemToAdd]);
    updateScreen("Menu");
  }

  //console.log(watch("example")); // watch input value by passing the name of it

  return (
    <>
      <BackBtn updateScreen={updateScreen} />
      {/* "handleSubmit" will validate your inputs before invoking "onSubmit" */}
      <form onSubmit={handleSubmit(onSubmit)}>

        {item.modifiers.map((modifier) => (<HandleModifier key={"modifier-handle_" + modifier.id} modifier={modifier} register={register}/>))}

        <input type="submit" />
      </form>
    </>
  );
}

function BackBtn ({updateScreen}) {
  return (
    <button onClick={() => {updateScreen("Menu")}}>Back</button>
  )
}

function HandleModifier ({modifier,register}) {
  switch (modifier.type) {
    case "SIZE":
      return(<SizeModifier key={"Modifier_" + modifier.id} modifier={modifier} register={register} />)
      break;
    default:
      return(<RadioModifier key={"Modifier_" + modifier.id} modifier={modifier} register={register} />)
  }
}

function SizeModifier ({modifier,register}) {
  //placeholder for size modifier

  // Select Medium size by default
  //[selectedSize, updateSelectedSize] = useState("Medium 12oz");
  return (
    <>
    <div>{modifier.title}</div>
    <div>
      {modifier.options.map((option) => (<label key={option.id}>
          <input  type="radio"
                {...register(modifier.inputName)}
                value={option.title}/>
          {option.title}
        </label>))}
    </div>
    </>
  )
}

function RadioModifier ({modifier,register}) {
  //placeholder for radio modifier
  return (
    <>
      <div>{modifier.title}</div>
        {modifier.options.map((option) => (<label key={option.id}>
          <input type="radio" {...register(modifier.inputName)} value={option.title} />
          {option.title}
        </label>))}
    </>
  )
}
