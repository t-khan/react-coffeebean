import React, {useState,useEffect} from 'react';
import { useForm } from "react-hook-form";
import './item.css';

function BackBtn ({updateScreen}) {
  return (
    <button className="item-back-button" onClick={() => {updateScreen("Menu")}}>Back</button>
  )
}

function ItemScreen ({item,updateScreen, cart, updateCart}) {
  /* Modifier Form thing-a-ma-jig */
  const {register, handleSubmit, watch, formState: {errors}} = useForm();

  const onSubmit = (data) => {
    /*
      Executed when add-to-cart-button is clicked
      Iterates over the item.modifiers and item.modifiers.Options
      and removes options that were not selected by the user.
    */

    //create a deep copy of item JSON
    let itemToAdd = JSON.parse(JSON.stringify(item));

    //filter item options based on user's selection
    for (var key in data) {
      for (var i = 0; i < itemToAdd.modifiers.length; i++) {
        if(itemToAdd.modifiers[i].inputName === key) {
          itemToAdd.modifiers[i].options = itemToAdd.modifiers[i].options.filter(option => option.title === data[key]);
        }
      }
    }

    //update cart with the item
    updateCart([...cart, itemToAdd]);
    console.log([...cart, itemToAdd]);
    updateScreen("Menu");
  }

  return(
    <>
    <ItemTopContainer itemTitle={item.title}
                      itemSubtitle={item.subtitle}
                      itemImg={item.image}
                      updateScreen={updateScreen}/>

    {/* Adding form here, will refactor into components later */}
    <div className="item-modifiers-scroll-container">
      <form onSubmit={handleSubmit(onSubmit)}
            className="item-modifiers-form">
        {item.modifiers.map((modifier) => (
          <HandleModifier key={"modifier_handle_" + modifier.id}
                          modifier={modifier}
                          register={register}/>
        ))}
        <div>
          <button className="item-add-button" type="submit">Add to Cart</button>
        </div>
      </form>
    </div>
    </>
  )
}

function ItemTopContainer ({itemTitle, itemSubtitle, itemImg, updateScreen}) {
  return (
    <>
      <div className="item-top-container">
        <BackBtn updateScreen={updateScreen} />
        <img  className="item-image"
              src={itemImg}/>
        {/* @TODO:  Combine title/subtitle in container with background gradient*/}
        <div className="item-title">
          {itemTitle}
        </div>
        <div className="item-subtitle">
          {itemSubtitle}
        </div>
      </div>
    </>
  )
}

function HandleModifier ({modifier,register}) {
  switch (modifier.type) {
    case "SIZE":
      return(<SizeModifier key={"Modifier_" + modifier.id} modifier={modifier} register={register} />)
      break;
    default:
      return(<RadioModifier key={"Modifier_" + modifier.id} modifier={modifier} register={register} />)
  }
}

function SizeModifier ({modifier,register}) {
  //placeholder for size modifier

  // Select Medium size by default
  const [selectedSize, updateSelectedSize] = useState("Medium 12oz");
  return (
    <>
    <div className="item-modifier-title">{modifier.title}</div>
    <div className="item-modifier-options-container">
      {modifier.options.map((option) => (<label key={option.id}
                                                className="item-modifier-size-option-label">
          <input  type="radio"
                  className="item-modifier-size-option-input"
                {...register(modifier.inputName)}
                value={option.title}
                checked={option.title === selectedSize}/>
          <a    className={"item-modifier-size-option-button " + (option.title == selectedSize? "size-selected" : "")}
                onClick={()=>{updateSelectedSize(option.title)}}>
            {option.title}
          </a>
        </label>))}
    </div>
    </>
  )
}

function RadioModifier ({modifier,register}) {
  //placeholder for radio modifier
  return (
    <>
      <div className="item-modifier-title">{modifier.title}</div>
      <div className="item-modifier-options-container">
        {modifier.options.map((option) => (<div key={option.id} className="item-modifier-radio-option">
          <label className="item-modifier-radio-option-label">
            <input type="radio" {...register(modifier.inputName)} value={option.title} />
            {option.title}
          </label>
        </div>))}
      </div>
    </>
  )
}


export default ItemScreen
