import React from 'react';

export default function ReviewScreen ({updateScreen,cart}) {
  return (
    <>
      <button onClick={() => updateScreen("Menu")}>Back</button>
      <div>In review Cart</div>
      <ReviewItems cart={cart} />
    </>
  )
}

function ReviewItems ({cart}) {
  if (cart.length == 0) {
    //cart is empty
    return(<EmptyCartScreen />)
  }
  else {
    //cart has items inside, show items
    return (
      cart.map((item, index) => (<div key={index}>{item.title}</div>))
    )
  }
}

function EmptyCartScreen () {
  return (
    <>
      <div>The cart is empty</div>
    </>
  )
}
