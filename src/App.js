import React, {useState,useEffect} from 'react'
import './App.css';
import MenuScreen from './Menu.js'
import ItemScreen from './Item.js'
import ReviewScreen from './Review.js'
import Splash from './Splash.js'

//Test Component
import Test from "./Test.js"
function App() {

  const [showScreen, updateScreen] = useState("Splash");
  const [showItem, updateShowItem] = useState({});

  // Getting Menu JSON object from application api

  //error checking and handling
  const [error,setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  //state to hold returned categories
  const [categories, setCategories] = useState([]);

  //state to hold items in cart
  const [cart, updateCart] = useState([]);

  //Advance to Menu screen after 5 seconds
  useEffect(()=>{
    //advance from splash Screen
    setTimeout(() => {
      updateScreen("Menu");
    }, 5000);

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
    };
    fetch('https://www.api.tofikkhan.com/demo/coffeebean/menu.php', requestOptions)
        .then(response => response.json())
        .then((result) => {
          setIsLoaded(true);
          setCategories(result);
        },
      (error) => {
        setIsLoaded(true);
        setError(error);
      });
  }, []);

  switch (showScreen) {
    case "Splash":
      return (
        <>
          <Splash />
        </>);
      break;
    case "Menu":
      if(error)
      {
        return (<div>Error: {error.message}</div>);
      }
      else if (!isLoaded)
      {
        return (<div>Loading...</div>);
      }
      else
      {
        return (
          <>
            <MenuScreen categories={categories}
                  updateScreen={updateScreen}
                  updateShowItem={updateShowItem}/>
          </>
        );
      }
      break;
      case "Item":
        return (
            <ItemScreen item={showItem}
                        updateScreen={updateScreen}
                        cart={cart}
                        updateCart={updateCart}/>
        );
        break;
      case "Review":
        return (
          <ReviewScreen updateScreen={updateScreen}
                        cart={cart}/>
        )
        break;
      case "Test":
        return (
          <Test item={categories[0].items[0]}
                updateScreen={updateScreen}
                cart={cart}
                updateCart={updateCart} />
        )
  }
}

export default App;
