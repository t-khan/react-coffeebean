import "./Splash.css"
import logo from "./imgs/logo-white.png";

function Splash() {
  return(
    <>
      <div className="splashBackground"></div>
      <div className="splashContainer">
        <div className="splashContent">
          <img src={logo} className="splashIcon" />
          <h1 className="splashTitle">
            CoffeeBean
          </h1>
        </div>
      </div>
    </>
  )
}

export default Splash;
